/* 
* Pipeline name: NIPT for N2K (50 bp)
* Version: 1.0
* Release date: 05/05/2021
* Author: Nguyen Thanh Dat

* How to run directive pipeline
cd path/to/RUN
nextflow run /mnt/GS_NAS02/Documents/Dat/pipeline/nipt/nextflow/niptpipeline_n2k.nf \
-with-report report.html -w ../work -c /mnt/GS_NAS02/Documents/Dat/pipeline/nipt/nextflow/niptpipeline_n2k.config

*/
params.DATA     = "$params.PP_REPO/ts_50bp"
SOURCECODE_FILE = file("$params.DATA")
PATIENT_INFO    = file("$params.PP_REPO/PatientInfo")
ADAPTER_FILE    = file("$params.PP_REPO/resources/adapter/TruSeq3-PE-2.fa")
INDEX_FILE      = file("$params.PP_REPO/resources/bt2")
RA1             = file("$params.DATA/refsets/NIPTeR_refset_347tr50_EUP.rds")
RS1             = file("$params.DATA/refsets/NIPTeR_refset_347tr50_EUP_XX.rds")
RA2             = file("$params.DATA/refsets/NIPTeR_refset_N2KEUP.rds")
RS2             = file("$params.DATA/refsets/NIPTeR_refset_N2KEUPXX.rds")
NCV1            = file("$params.DATA/refsets/NIPTeR_refset_347tr50_EUP.ncvtemplate.rds")
NCV2            = file("$params.DATA/refsets/NIPTeR_refset_N2KEUP.ncvtemplate.rds")
QDNA_FILE       = file("$params.DATA/refsets/QDNAseq.hg19.bin100kb.rds")
WCX_REF         = file("$params.DATA/refsets/wcxN2K.npz")
REFFILE         = file("$params.DATA/refsets/refsets_TSDP_TSHG_TScp50_TSlp50_CBb1k_CBlp50_sexref_TS347_TS347sex_TS347g_TS347c_TSN2K_TSN2Ksex.txt")
FUNC            = file("$params.DATA/triSure_functions.R")

// param dir
SEQFF              = file("$params.DATA/SeqFF")
FRAGSIZE_DIR       = file("$params.OUTDIR/fragsize")
NIPTER_DIR         = file("$params.OUTDIR/nipter24")
ALIGN_DIR          = file("$params.OUTDIR/align")
PATIENT_REPORT_DIR = file("$params.OUTDIR/patient_reports")
WCX_RESULT_DIR     = file("$params.OUTDIR/wcx_results")
WCX_REPORT_DIR     = file("$params.OUTDIR/wcx_report")
TEMPLATE_DIR       = file("$params.DATA/report_templates")
FQDIR_PATH = file("$params.FQDIR")
//--------------------------------------- PROCESS ------------------------------------------------//
Channel
    .fromFilePairs("$params.FQDIR/*_R{1,2}*.fastq.gz")
    .ifEmpty { error "Cannot find any reads matching: ${params.FQDIR}/*_R{1,2}*" }
    .into {Fastqc_ch; Dedupfastq_ch}


process FastQC {
    cache "deep"; tag "$sample_id"
    //errorStrategy 'retry'
    //maxRetries 1
    //storeDir "$params.OUTDIR/fastqc"
    //cpus params.FASTQC_THREAD
    //maxForks 10
    
    input:
        tuple sample_id, file(READS) from Fastqc_ch
    output:
        tuple sample_id, "${sample_id}_R1*fastqc.html", "${sample_id}_R2*fastqc.html"
    script:
        "fastqc -t $params.FASTQC_THREAD --quiet --outdir . ${READS[0]} ${READS[1]}"
}

/*
process Dedupfastq {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'
    maxRetries 3
    storeDir "$params.OUTDIR/dedup"
    maxForks 2
    input:
        tuple sample_id, file(READS) from Dedupfastq_ch
    output:
        tuple sample_id, "${sample_id}_R*.dedup.fastq.gz" into Trim_ch, Trim_Wcx_ch
    script:
        """
        clumpify.sh in=${READS[0]} in2=${READS[1]} \
        out=${sample_id}_R1.dedup.fastq.gz out2=${sample_id}_R2.dedup.fastq.gz dedupe subs=0
        """
}

process Trim {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'
    maxRetries 3
    storeDir "$params.OUTDIR/trim"
    cpus params.TRIM_THREAD
    maxForks 1
    
    input:
        tuple sample_id, file(READS) from Trim_ch
        file ADAPTER_FILE
    output:
        tuple sample_id, "${sample_id}_R{1,2}_trim50.fastq.gz" into Align_ch   
    script:
       """
       trimmomatic PE -phred33 -threads $params.TRIM_THREAD ${READS[0]} ${READS[1]} \
       ${sample_id}_R1_trim50.fastq.gz ${sample_id}_R1_UP.fastq.gz \
       ${sample_id}_R2_trim50.fastq.gz ${sample_id}_R2_UP.fastq.gz \
       ILLUMINACLIP:${ADAPTER_FILE}:2:30:10 LEADING:3 TRAILING:3 \
       SLIDINGWINDOW:4:15 CROP:$params.READLENGTH MINLEN:36
       """
}

process Align {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'   
    maxRetries 3
    storeDir "$params.OUTDIR/align"
    cpus params.ALIGN_THREAD
    maxForks 1
    
    input:
        tuple sample_id, file ("${sample_id}_R*_trim50.fastq.gz") from Align_ch
        file INDEX_FILE
    output:
        tuple sample_id, "${sample_id}.sam" into SortBam_ch
    
    script:
        """
        bowtie2 --threads $params.ALIGN_THREAD --mm \
        --very-fast \
        --no-mixed \
        --no-discordant \
        --no-unal \
        --rg-id ${sample_id} \
        --rg SM:${sample_id} \
        -x ${INDEX_FILE}/hg19 \
        -1 ${sample_id}_R1_trim50.fastq.gz -2 ${sample_id}_R2_trim50.fastq.gz \
        -S ${sample_id}.sam
        """
}

process SortBam {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'
    maxRetries 3
    storeDir "$params.OUTDIR/align"
    cpus params.SAMTOOLSORT_THREAD
    maxForks 1
    
    input:
        tuple sample_id, file(BAM) from SortBam_ch
    output:
        tuple sample_id, "${sample_id}.sorted.{bam,bam.bai}" into Nipter_ch, ExtractFragsize_ch, ExtractSmallFragment_ch, GenSexcall_ch
    script:
        """
        samtools sort --threads $params.SAMTOOLSORT_THREAD -m 2G ${BAM[0]} -o ${sample_id}.sorted.bam 
        samtools index -@ $params.SAMTOOLSORT_THREAD ${sample_id}.sorted.bam
        """
}

process ExtractFragsize {
    cache "deep"; tag "$sample_id"
    storeDir "$params.OUTDIR/fragsize"
    errorStrategy 'retry'
    maxRetries 2
    cpus params.FRAGSIZE_THREAD
    maxForks 1
    
    input: 
        tuple sample_id, file(SORTEDBAMs) from ExtractFragsize_ch
    output: 
        file "${sample_id}.fragsize.txt" into TrisureR_ch1
    script:
        """
        samtools view --threads $params.FRAGSIZE_THREAD -q 10 ${SORTEDBAMs[0]} \
        | awk '{if(\$9 >= 1) print \$3 \"\\t\" \$9}' > ${sample_id}.fragsize.txt
        """
}

process ExtractSmallFragment {
    cache "deep"; tag "$sample_id"
    storeDir "$params.OUTDIR/fragsize"
    errorStrategy 'retry'
    maxRetries 2
    cpus params.FRAGSIZE_THREAD
    maxForks 1
    
    input:
        tuple sample_id, file(SORTEDBAMs) from ExtractSmallFragment_ch
    output: 
        file "${sample_id}.{sam,150.bam}" into TrisureR_ch2
    script:
        """
        samtools view --threads $params.FRAGSIZE_THREAD -H ${SORTEDBAMs[0]} > ${sample_id}.header
        samtools view --threads $params.FRAGSIZE_THREAD -f2 ${SORTEDBAMs[0]} \
        | awk '(\$9 >= 1 && \$9 <= 150) { print; }' | cat ${sample_id}.header - > ${sample_id}.sam
        samtools view --threads $params.FRAGSIZE_THREAD -bS ${sample_id}.sam -o ${sample_id}.150.bam
        """
}

// /opt/miniconda2/envs/Rv3.6/bin/R
// install.packages("NIPTeR")
// install.packages("optparse")
// install.packages("tidyverse")

process Nipter {
    cache "deep"; tag "$sample_id"
    storeDir "$params.OUTDIR/nipter24"
    errorStrategy 'retry'
    maxRetries 2
    cpus params.NIPTER_THREAD
    maxForks 1
    
    input: 
        tuple sample_id, file(SORTEDBAMs) from Nipter_ch
        file SOURCECODE_FILE
        file RA1
        file RS1
        file RA2
        file RS2
        file NCV1
        file NCV2
    output:
        file "${sample_id}.nipter*.txt" into TrisureR_ch3 
    script:
        """
        /usr/bin/Rscript ${SOURCECODE_FILE}/nipter24_3calls_50bp.R \
        -i ${SORTEDBAMs[0]} \
        --ra1 ${RA1} --rs1 ${RS1} \
        --ra2 ${RA2} --rs2 ${RS2} \
        --ncv1 ${NCV1} --ncv2 ${NCV2} \
        --outdir .
        """
}

// install.packages("furrr")
// install.packages("openxlsx")

process Report {
    cache "deep"; tag "$sample_id"
    publishDir "$params.OUTDIR/report", mode: 'copy'
    errorStrategy 'retry'
    maxRetries 2
    cpus params.REPORT_THREAD
    maxForks 1
    
    input: 
        file("*") from TrisureR_ch1.collect()
        file("*") from TrisureR_ch2.collect()
        file("*") from TrisureR_ch3.collect()
        file SOURCECODE_FILE
        file FRAGSIZE_DIR
        file NIPTER_DIR
        file ALIGN_DIR
        file REFFILE
        file SEQFF
        file FUNC
    output:
        file "${params.RUN}_*.{fragsizecounts,results,xlsx}" into PatientReport_ch, DoctorReport_ch, Result_ch
    script:
        """
        /usr/bin/Rscript ${SOURCECODE_FILE}/trisure24_50bp.R \
        -i ${FRAGSIZE_DIR}/ \
        --nipter ${NIPTER_DIR}/ \
        --align ${ALIGN_DIR} \
        --ref $REFFILE \
        -o ${params.RUN}_${params.PANEL} \
        --outdir .
        """
}

process Trim_Wcx {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'
    maxRetries 2
    storeDir "$params.OUTDIR/trim_wcx"
    cpus params.TRIM_THREAD
    maxForks 1
    
    input:
        tuple sample_id, file(READS) from Trim_Wcx_ch
        file ADAPTER_FILE
    output:
        tuple sample_id, "${sample_id}_R*_trim50.fastq.gz" into Align_samblaster_ch   
    script:
       """
       trimmomatic PE -phred33 -threads $params.TRIM_THREAD ${READS[0]} ${READS[1]} \
       ${sample_id}_R1_trim50.fastq.gz ${sample_id}_R1_UP.fastq.gz \
       ${sample_id}_R2_trim50.fastq.gz ${sample_id}_R2_UP.fastq.gz \
       ILLUMINACLIP:${ADAPTER_FILE}:2:30:10 MINLEN:36
       """
}

process Align_Wcx {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'   
    maxRetries 3
    storeDir "$params.OUTDIR/align_wcx"
    cpus params.ALIGN_WCX_THREAD
    maxForks 1
    
    input:
        tuple sample_id, file(READS) from Align_samblaster_ch
        file INDEX_FILE
    output:
        tuple sample_id, "${sample_id}.sam" into SortBam_Wcx_ch
    
    script:
        """
        bowtie2 --threads $params.ALIGN_WCX_THREAD --mm \
        --local \
        --no-unal \
        --rg-id ${sample_id} \
        --rg SM:${sample_id} \
        -x ${INDEX_FILE}/hg19 \
        -U ${READS[0]} \
        | samblaster -o ${sample_id}.sam
        """
}

process SortBam_Wcx {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'
    maxRetries 2
    storeDir "$params.OUTDIR/align_wcx"
    cpus params.SAMTOOLSORT_THREAD
    maxForks 1
    
    input:
        tuple sample_id, file(BAM) from SortBam_Wcx_ch
    output:
        tuple sample_id, "${sample_id}.dedup.{bam,bam.bai}" into BamtoNPZ_ch
    script:
        """
        samtools sort --threads $params.SAMTOOLSORT_THREAD -m 2G ${BAM[0]} -o ${sample_id}.dedup.bam 
        samtools index -@ $params.SAMTOOLSORT_THREAD ${sample_id}.dedup.bam
        """
}

process BamtoNPZ {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'
    maxRetries 2
    storeDir "$params.OUTDIR/npz"
    cpus 2
    maxForks 1
    
    input:
        tuple sample_id, file(DEDUP) from BamtoNPZ_ch
    output:
        tuple sample_id, file ("${sample_id}.dedup.npz") into WisecondorX_ch
    script:
        """
        WisecondorX convert --binsize 5e3 ${DEDUP[0]} ${sample_id}.dedup.npz
        """
}

// conda create --name wisecondorx_py27  python=2.7
// conda activate wisecondorx_py27
// conda install wisecondorx
// BiocManager::install("DNAcopy")
// Note: need to create folder "wcx_results" before
// conda activate Rv3.6

process WisecondorX {
    cache "deep"; tag "$sample_id"
    errorStrategy 'retry'
    maxRetries 2
    storeDir "$params.OUTDIR/wcx_results"
    cpus 2
    maxForks 1
    
    input:
        tuple sample_id, file ("${sample_id}.dedup.npz") from WisecondorX_ch
        file WCX_REF
        file WCX_RESULT_DIR
    output:
        file "${sample_id}*" into Wcxts_combinedreport_ch
    script:
        """
        mkdir -p $params.OUTDIR/wcx_results
        WisecondorX predict ${sample_id}.dedup.npz ${WCX_REF} ${WCX_RESULT_DIR}/${sample_id}.dedup.npz --bed --plot && \
        WisecondorX gender ${sample_id}.dedup.npz ${WCX_REF} > ${sample_id}.dedup.npz.sex
        """
}

//  apt-get install pandoc
// install.packages("ggrepel")
// install.packages("plotly")
// install.packages('caret', dependencies = TRUE)
// Note: Need to create wcx_report before
// WCX_REPORT_DIR is an absolute dir

process CombineReportTS_WCX {
    cache "deep";
    errorStrategy 'retry'
    maxRetries 2
    publishDir "$params.OUTDIR/wcx_report", mode: 'copy'
    cpus params.REPORT_THREAD
    maxForks 1
    
    input:
        file(RS) from Result_ch
        file(F) from Wcxts_combinedreport_ch.collect().toList()
        file SOURCECODE_FILE
        file WCX_RESULT_DIR
        file WCX_REPORT_DIR
        file QDNA_FILE

    output:
        // file "${params.RUN}_${params.PANEL}_wcx.pdf"
    script:
    if (params.PPname == 'ts3' || params.PPname == 'ts95')
        """
        mkdir -p $params.OUTDIR/wcx_report
        /usr/bin/Rscript ${SOURCECODE_FILE}/ts_wcx_runreport_50bp.R \
        -i ${WCX_RESULT_DIR} \
        --ts $params.OUTDIR/report/${RS[1]} \
        --batch $params.RUN \
        -o ${params.RUN}_${params.PANEL}_wcx \
        --outdir $params.OUTDIR/${WCX_REPORT_DIR} \
        --qdna ${QDNA_FILE} \
        --template ${SOURCECODE_FILE}/trisure3wcx_50bp_batch_report.Rmd
        """
    else if(params.PPname == 'ts24')
        """
        mkdir -p $params.OUTDIR/wcx_report
        /usr/bin/Rscript ${SOURCECODE_FILE}/ts_wcx_runreport_50bp.R \
        -i ${WCX_RESULT_DIR} \
        --ts $params.OUTDIR/report/${RS[1]} \
        --batch $params.RUN \
        -o ${params.RUN}_${params.PANEL}_wcx \
        --outdir $params.OUTDIR/${WCX_REPORT_DIR} \
        --qdna ${QDNA_FILE} \
        --template ${SOURCECODE_FILE}/trisure24wcx_50bp_batch_report.Rmd
        """
    else "echo Do not thing"
}

*/
// export LD_LIBRARY_PATH="/usr/lib/libreoffice/program:$LD_LIBRARY_PATH"
// export LD_LIBRARY_PATH="/usr/lib/soffice/program:$LD_LIBRARY_PATH"
// cd /media/genesolutions/DELL02_HD01/DAT/TestNIPT/NIPT3_N2K
// /usr/bin/Rscript  ../../src/generate_trisure3patientreport.R \
// -t ../../data/report_templates \
// -p ../../data/NIPT_commercial.xlsx \
// -i report/R591_NIPT3_N2K.results \
// -o patient_reports \
// -l /usr/bin/soffice -c "pdf"

/*
process GenPatientReport {
    publishDir "$params.OUTDIR/patient_reports", mode: 'copy'
    errorStrategy 'retry'
    maxRetries 2
    cpus 2
    maxForks 1
    input: 
        file(RESULT) from PatientReport_ch
    output: 
        file ("*.{xlsx,pdf}")
    script:
    if (params.PPname == 'ts3')
        """
        $params.RSCIPT_ENV ${GENREPORT_SRC}/generate_trisure3patientreport.R \
        -t ${TEMPLATE_DIR} \
        -p ${PATIENT_INFO}/NIPT_commercial_${params.RUN}.xlsx \
        -i ${RESULT[1]} \
        -o . \
        -l $params.OFFICE -c pdf
        """
    else if (params.PPname == 'ts24')
        """
        $params.RSCIPT_ENV ${GENREPORT_SRC}/generate_trisure24patientreport.R \
        -t ${TEMPLATE_DIR} \
        -p ${PATIENT_INFO}/NIPT_commercial_${params.RUN}.xlsx \
        -i ${RESULT[1]} \
        -o . \
        -l $params.OFFICE -c pdf
        """
    else if (params.PPname == 'ts95')
        """
        $params.RSCIPT_ENV ${GENREPORT_SRC}/generate_nipt95patientreport.R \
        -t ${TEMPLATE_DIR} \
        -p ${PATIENT_INFO}/NIPT_commercial_${params.RUN}.xlsx \
        -i ${RESULT[1]} \
        -o . \
        -l $params.OFFICE -c pdf
        """
    else "echo Do not thing"
}

process GenDoctorReport {
    publishDir "$params.OUTDIR/patient_reports", mode: 'copy'
    errorStrategy 'retry'
    maxRetries 2
    cpus 2
    maxForks 1
    input: 
        file(RESULT) from DoctorReport_ch
    output: 
        file ("*.{xlsx,pdf}")
    script:
    if (params.PPname == 'ts3')
        """
        $params.RSCIPT_ENV ${GENREPORT_SRC}/generate_trisure3doctorreport.R \
        -t ${TEMPLATE_DIR} \
        -p ${PATIENT_INFO}/NIPT_commercial_${params.RUN}.xlsx \
        -i ${RESULT[1]} \
        -o . \
        -l $params.OFFICE -c pdf
        """
    else if (params.PPname == 'ts24')
        """
        $params.RSCIPT_ENV ${GENREPORT_SRC}/generate_trisure24doctorreport.R \
        -t ${TEMPLATE_DIR} \
        -p ${PATIENT_INFO}/NIPT_commercial_${params.RUN}.xlsx \
        -i ${RESULT[1]} \
        -o . \
        -l $params.OFFICE -c pdf
        """
    else if (params.PPname == 'ts95')
        """
        $params.RSCIPT_ENV ${GENREPORT_SRC}/generate_nipt95doctorreport.R \
        -t ${TEMPLATE_DIR} \
        -p ${PATIENT_INFO}/NIPT_commercial_${params.RUN}.xlsx \
        -i ${RESULT[1]} \
        -o . \
        -l $params.OFFICE -c pdf
        """
    else "echo Do not thing"
}
*/
