FROM amazoncorretto:17.0.5
RUN yum install -y procps-ng shadow-utils

ENV NXF_HOME=/.nextflow
ARG TARGETPLATFORM=linux/amd64

COPY .aws /root/.aws

# copy docker client
COPY dist/${TARGETPLATFORM}/docker /usr/local/bin/docker

RUN curl -s https://get.nextflow.io | bash \
 && mv nextflow /usr/local/bin/

# download runtime
RUN touch /.nextflow/dockerized \
 && chmod 755 /usr/local/bin/nextflow \
 && nextflow info

RUN yum install -y git python-pip curl jq
RUN pip install --upgrade awscli
COPY niptmain_aws.sh /usr/local/bin/niptmain_aws.sh
VOLUME ["/scratch"]
CMD ["/usr/local/bin/niptmain_aws.sh"]
