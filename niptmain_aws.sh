#!/bin/bash

# Parameters neeed to pass
RUN=${RUN}
NF_SCRIPT=${NF_SCRIPT}
NF_CONFIG=${NF_CONFIG}
# ts=(${2-ts*})

year=$(date +"%Y")
datehour=$(date '+%Y-%m-%d_%H-%M-%S')
reviewdir=/volume1/REVIEW_QC/NIPT/$year/$RUN
reportdir=/volume1/PATIENT_REPORT/NIPT/$year/$RUN
dashboard_dir=$WORKDIR/$RUN 
status3=2; status24=2; status95=2
n2k="_N2K"
git clone https://gitlab.com/todachuy2406/nextflow-repo.git /scratch
nextflowfile=niptpipeline_n2k_aws.nf
configfile=niptpipeline_n2k_aws.config
# -------------------------------------------------------------------------------------------------------------#
# check exist fastq file in this run
    work=$WORKDIR/$RUN/work
    #mkdir -p $WORKDIR/$RUN; cd $WORKDIR/$RUN
    mkdir -p /scratch/logs

    #vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv Call nextfllow pipeline vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv#
    #-------------------------------------Trisure 3-------------------------------------# 
    echo "--------- Running: $RUN ---------"
    echo -----------------------TRISURE3---------------------------
    echo $datehour
    echo $NF_SCRIPT
    echo $NF_CONFIG
    echo $RUN
        mkdir -p $work/${RUN}_ts3
        PANEL=NIPT3$n2k

        #nextflow run $SOURCEDIR/nextflow/$nextflowfile \
        nextflow run /scratch/$NF_SCRIPT \
        -c /scratch/$NF_CONFIG \
        -with-timeline s3://s3data500g/report/${datehour}_${RUN}_timeline.html \
        -with-report s3://s3data500g/report/${datehour}_${RUN}_report.html \
        --RUN       $RUN     \
        --PANEL     $PANEL \
        --PPname    "ts3"  \
        -bucket-dir s3://s3data500g/work/${datehour}_${RUN} \
        --output    s3://s3data500g/results/${datehour}_${RUN} \
        -profile batch
    #-with-trace $dashboard_dir/${RUN}+Trisure3+$(hostname).trace.tsv \
        status3="$?"
        if [ $status3 == "0" ];then
            echo $PANEL run is successful >> /scratch/logs/${datehour}_${RUN}_status.log
            aws s3 cp /scratch/logs/${datehour}_${RUN}_status.log s3://s3data500g/logs
            # python3 $SOURCEDIR/utils/insert_dashboard.py --run $RUN \
            # --panel "Trisure3" --fqdir ./raw/ts3 --iscomplete True
            
        else
            echo $PANEL run on is fail >> /scratch/logs/${datehour}_${RUN}_status.log
            aws s3 cp /scratch/logs/${datehour}_${RUN}_status.log s3://s3data500g/logs
        fi
